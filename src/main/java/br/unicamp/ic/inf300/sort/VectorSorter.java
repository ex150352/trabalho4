package main.java.br.unicamp.ic.inf300.sort;


import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;


public class VectorSorter {
	
	final static Logger logger = Logger.getLogger(VectorSorter.class);
	
	public static void main(String[] args) {
		
		BasicConfigurator.configure();
	
		int[] numbers = parseParameters(args);			
		
		logger.info("Input: ");
		printVector(numbers);
		sort(numbers);
		logger.info("Sorted: ");
		printVector(numbers);
	}
	
	public static void sort(int[] numbers) {
		BubbleSort.sort(numbers);
	}
	
	public static int[] parseParameters(String[] args) {
		int[] numbers;
		if(args.length > 0) {
			numbers = new int[args.length];
			for(int k=0; k<args.length; k++) {
				numbers[k] = Integer.parseInt(args[k]);
			}
		}else {
			numbers = generateRandomVector(10);
		}
		return numbers;
	}
	
	private static int[] generateRandomVector(int size) {

		int[] vector = new int[size];

		for (int i = 0; i < vector.length ; i++) {
			vector[i] = (int) (Math.random()*100 + 1);
		}
		
		return vector;
	}
	
	public static void printVector(int[] numbers) {
		String message = "[ ";
	
		message = message + String.valueOf(numbers[0]);
		
		int i = 0;
		do {
			i++;
			message = message + ", ";
			message = message + String.valueOf(numbers[i]);
		}while(i < numbers.length - 1);

		message = message + " ]";
		logger.info(message);
	}
}
